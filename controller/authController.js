const User = require("../model/User");
const jwt = require('jsonwebtoken')

// handle errors
const handleErrors = (err) => {
    console.log(err.message, err.code);
    let errors = { email: '', password: '' };
    // duplicate email error
    if (err.code === 11000) {
        errors.email = 'that email is already registered';
        return errors;
    }
    // validation errors
    if (err.message.includes('user validation failed')) {
        // console.log(err);
        Object.values(err.errors).forEach(({ properties }) => {
            // console.log(val);
            // console.log(properties);
            errors[properties.path] = properties.message;
        });
    }
    return errors;
}


// controller actions 

module.exports.login_post = async (req, res) => {
    const { email, password } = req.body;
    try {
        const user = await User.find({ email, password });
        if (user) {
            const token = createToken(user._id);
            res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 });
            res.status(201).json({ user: user._id });
        }

        if(email is not exist){
            res.status(201).json({message: 'User not found (invalid email)'});
        }
        if(password is not exist){
            res.status(201).json({message: 'Incorrect password'});
        }
        
        
    } catch (err) {
        const errors = handleErrors(err);
        res.status(400).json({ errors });
    }
} 
